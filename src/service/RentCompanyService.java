package service;

import java.time.LocalDate;

import db.DriversRepository;
import db.IRepository;
import db.entities.DriverEntity;
import ui.DriversState;

public class RentCompanyService {
	IRepository<DriverEntity, Integer> driverRepository = new DriversRepository();

	public DriversState addDriver(int licenseId, String name, LocalDate birthDate, String phone) {
		if (driverRepository.existsById(licenseId))
			return DriversState.ALL_READY_EXISTS;
		DriverEntity driverEntity = new DriverEntity(licenseId, name, birthDate, phone);
		driverEntity = driverRepository.save(driverEntity);
		if (driverEntity == null)
			return DriversState.ERROR;
		return DriversState.OK;
	}
	
//	public DriverDto getDriverById(int id) {
//		// driverEntity -> DriverDto
//	}
}
