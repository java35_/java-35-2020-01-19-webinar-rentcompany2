package db;

import java.util.List;

import db.entities.DriverEntity;

public class DriversRepository implements IRepository<DriverEntity, Integer> {

	@Override
	public DriverEntity findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DriverEntity save(DriverEntity entity) {
		return rentCompany.saveDriver(entity);
	}

	@Override
	public List<DriverEntity> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existsById(Integer id) {
		return rentCompany.existsByDriverId(id);
	}
}
